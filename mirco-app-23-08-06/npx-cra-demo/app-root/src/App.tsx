import React from 'react';
import microApp from '@micro-zoe/micro-app'
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <micro-app name="app-dashboard" url="http://localhost:3001/" baseroute='/dashboard' />
      </header>
    </div>
  );
}

export default App;
